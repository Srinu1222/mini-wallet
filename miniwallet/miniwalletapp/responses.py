from django.http import JsonResponse


def send_fail_http_response(data=None):
    args = {'status': 'FAIL', 'data': data}
    return JsonResponse(args, status=400)


def send_pass_http_response(data=None):
    args = {'status': 'success', 'data': data}
    return JsonResponse(args)
