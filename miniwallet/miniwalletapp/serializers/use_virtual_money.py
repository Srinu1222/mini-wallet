from django.db import transaction, IntegrityError
from miniwalletapp.models import WithDrawl
from miniwalletapp.responses import send_fail_http_response, send_pass_http_response
from miniwalletapp.services import handle_wallet_not_enabled


def use_virtual_money_serializer(wallet, amount, reference_id):

    if not wallet:
        return send_fail_http_response(data='Wallet not found for this user')

    is_wallet_not_enabled = handle_wallet_not_enabled(wallet)
    if is_wallet_not_enabled:
        return send_fail_http_response(data='Wallet Needs to be enabled in order to use the balance')

    with transaction.atomic():

        current_balance = wallet.balance
        is_current_amount_more_than_or_equal_to_withdraw_amount = current_balance >= amount
        if not is_current_amount_more_than_or_equal_to_withdraw_amount:
            return send_fail_http_response(
                data='With-drawl amount is exceeded than current balance.')

        current_balance -= amount
        wallet.balance = current_balance
        wallet.save()

        try:
            withdraw = WithDrawl.objects.create(
                wallet=wallet,
                reference_id=reference_id,
                amount=amount
            )
        except IntegrityError:
            return send_fail_http_response(data='Reference Id already exists. Please made another withdraw')

    serialized_data = {
        "withdrawal": {
            "id": withdraw.id,
            "withdrawn_by": withdraw.wallet.owned_by.pk,
            "status": "success",
            "withdrawn_at": str(withdraw.withdrawn_at),
            "amount": wallet.balance,
            "reference_id": withdraw.reference_id
        }
    }

    return send_pass_http_response(data=serialized_data)
