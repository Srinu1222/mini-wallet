from django.db import transaction, IntegrityError
from miniwalletapp.models import Deposit
from miniwalletapp.responses import send_fail_http_response, send_pass_http_response
from miniwalletapp.services import handle_wallet_not_enabled


def add_wallet_balance_serializer(wallet, amount, reference_id):

    if not wallet:
        return send_fail_http_response(data='Wallet not found for this user')

    is_wallet_not_enabled = handle_wallet_not_enabled(wallet)
    if is_wallet_not_enabled:
        return send_fail_http_response(data='Wallet Needs to be enabled in order to add the balance')

    with transaction.atomic():

        is_amount_positive = amount >= 0
        if not is_amount_positive:
            return send_fail_http_response(data='Amount should be greater than zero')

        current_balance = wallet.balance
        current_balance += amount
        wallet.balance = current_balance
        wallet.save()

        try:
            deposit = Deposit.objects.create(
                wallet=wallet,
                reference_id=reference_id,
                amount=amount
            )
        except IntegrityError:
            return send_fail_http_response(data='Reference Id already exists. Please made another deposit')

    serialized_data = {
        "deposit": {
            "id": deposit.pk,
            "deposited_by": deposit.wallet.owned_by.pk,
            "status": "success",
            "deposited_at": str(deposit.deposited_at),
            "amount": wallet.balance,
            "reference_id": deposit.reference_id
        }
    }

    return send_pass_http_response(data=serialized_data)
