from django.contrib import admin

# Register your models here.
from miniwalletapp.models import *

admin.site.register(User)
admin.site.register(Account)
admin.site.register(Wallet)
admin.site.register(Deposit)
admin.site.register(WithDrawl)



