from django.db import models
from django.contrib.auth.models import AbstractUser, UserManager
from django.conf import settings
from django.db.models.signals import post_save
from django.dispatch import receiver
from rest_framework.authtoken.models import Token


@receiver(post_save, sender=settings.AUTH_USER_MODEL)
def create_auth_token(sender, instance=None, created=False, **kwargs):
    if created:
        Token.objects.create(user=instance)


class User(AbstractUser):
    customer_xid = models.CharField(max_length=100, null=True, blank=True)

    objects = UserManager()

    def __str__(self):
        return str(self.pk)


class Account(models.Model):
    user = models.ForeignKey('User', on_delete=models.CASCADE)

    def __str__(self):
        return self.user.customer_xid


class Wallet(models.Model):
    owned_by = models.ForeignKey('User', on_delete=models.CASCADE)
    status = models.BooleanField(default=False)
    enabled_at = models.DateTimeField(null=True, blank=True)
    balance = models.FloatField(default=0.00)
    disabled_at = models.DateTimeField(null=True, blank=True)

    def __str__(self):
        return self.owned_by.customer_xid


class Deposit(models.Model):
    wallet = models.ForeignKey('Wallet', on_delete=models.CASCADE)
    reference_id = models.CharField(max_length=1000, unique=True)
    amount = models.FloatField(default=0.00)
    deposited_at = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.reference_id


class WithDrawl(models.Model):
    wallet = models.ForeignKey('Wallet', on_delete=models.CASCADE)
    reference_id = models.CharField(max_length=1000, unique=True)
    amount = models.FloatField(default=0.00)
    withdrawn_at = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.reference_id
