from django.urls import path
from miniwalletapp.api.add_account import add_account
from miniwalletapp.api.UserWallet import UserWallet
from miniwalletapp.api.add_wallet_balance import AddMoneyToWallet
from miniwalletapp.api.use_virtual_money import UseVirtualMoney

urlpatterns = [
    path("v1/init", add_account, name="add_account"),
    path("v1/wallet", UserWallet.as_view(), name="enable_wallet"),
    path("v1/wallet/deposits", AddMoneyToWallet.as_view(), name="add_money_to_wallet"),
    path("v1/wallet/withdrawals", UseVirtualMoney.as_view(), name="use_virtual_money")
]
