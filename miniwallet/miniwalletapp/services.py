def handle_wallet_not_enabled(wallet):
    is_enabled = wallet.status
    return True if not is_enabled else False

