from datetime import datetime
from rest_framework.views import APIView
from rest_framework import authentication, permissions
from miniwalletapp.models import Wallet
from miniwalletapp.responses import send_fail_http_response, send_pass_http_response
from miniwalletapp.services import handle_wallet_not_enabled


class UserWallet(APIView):
    authentication_classes = (authentication.TokenAuthentication,)
    permission_classes = (permissions.IsAuthenticated,)

    def post(self, request):

        # To get the wallet for the requested user
        wallet = Wallet.objects.get(owned_by=request.user)

        # Wallet status
        is_enabled = wallet.status
        if is_enabled:
            return send_fail_http_response(data='Wallet is already enabled_for_this_user')

        wallet.enabled_at = datetime.now()
        wallet.status = True
        wallet.save()

        serialized_data = {
                "wallet": {
                    "id": wallet.pk,
                    "owned_by": wallet.owned_by.pk,
                    "status": "enabled",
                    "enabled_at": str(wallet.enabled_at),
                    "balance": wallet.balance
                }
            }

        return send_pass_http_response(data=serialized_data)

    def get(self, request):

        # To get the wallet for the requested user
        wallet = Wallet.objects.get(owned_by=request.user)

        is_wallet_not_enabled = handle_wallet_not_enabled(wallet)
        if is_wallet_not_enabled:
            return send_fail_http_response(data='Wallet Needs to be enabled in order to view the balance')

        serialized_data = {
                "wallet": {
                    "id": wallet.pk,
                    "owned_by": wallet.owned_by.pk,
                    "status": "enabled",
                    "enabled_at": str(wallet.enabled_at),
                    "balance": wallet.balance
                }
            }

        return send_pass_http_response(data=serialized_data)

    def patch(self, request):

        # To get the wallet for the requested user
        wallet = Wallet.objects.get(owned_by=request.user)

        is_disabled = request.POST['is_disabled']
        wallet.status = True if is_disabled == 'true' else False
        wallet.disabled_at = datetime.now()
        wallet.save()

        serialized_data = {
                "wallet": {
                    "id": wallet.pk,
                    "owned_by": wallet.owned_by.pk,
                    "status": "disabled",
                    "disabled_at": str(wallet.disabled_at),
                    "balance": wallet.balance
                }
            }

        return send_pass_http_response(data=serialized_data)


