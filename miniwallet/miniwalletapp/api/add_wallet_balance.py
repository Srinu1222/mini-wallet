from rest_framework.views import APIView
from rest_framework import authentication, permissions

from miniwalletapp.models import Wallet
from miniwalletapp.serializers.add_wallet_balance import add_wallet_balance_serializer


class AddMoneyToWallet(APIView):
    authentication_classes = (authentication.TokenAuthentication,)
    permission_classes = (permissions.IsAuthenticated,)

    def post(self, request):
        amount = float(request.POST['amount'])
        reference_id = request.POST['reference_id']
        wallet = Wallet.objects.filter(owned_by=request.user).first()
        serialized_response = add_wallet_balance_serializer(wallet, amount, reference_id)
        return serialized_response
