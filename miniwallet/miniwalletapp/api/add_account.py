from miniwalletapp.models import Account, User, Token, Wallet
from django.views.decorators.csrf import csrf_exempt
from miniwalletapp.responses import send_pass_http_response


@csrf_exempt
def add_account(request):
    if request.method == 'POST':
        customer_xid = request.POST['customer_xid']

        # To (create or get) the User for creating an account to the requested user
        user, is_new = User.objects.get_or_create(customer_xid=customer_xid)

        # To get the token for the user
        token = Token.objects.get(user=user).key

        # To create an Account for the requested user
        Account.objects.create(user=user)

        # To create a Wallet for the user
        Wallet.objects.create(owned_by=user)

        serialized_data = {"token": token}

        return send_pass_http_response(data=serialized_data)

