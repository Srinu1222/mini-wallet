from django.apps import AppConfig


class MiniwalletappConfig(AppConfig):
    name = 'miniwalletapp'
